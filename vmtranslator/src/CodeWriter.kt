import java.io.*


class CodeWriter(filePath: String) {

    private var pw: PrintWriter? = null
    private var fileDir: String? = null

    init {
        fileDir = filePath.split("/").subList(0, filePath.split("/").size - 1).joinToString("/")

        // 出力
        val fil = FileWriter(File(fileDir, filePath.split("/").last()))
        pw = PrintWriter(BufferedWriter(fil))

        pw!!.println("@256")
        pw!!.println("D=A")
        pw!!.println("@SP")
        pw!!.println("M=D")
    }

    fun setFileName(fileName: String) {

        // 一旦閉じて
        pw!!.close()

        // また再開
        val fil = FileWriter(File(fileDir, fileName))
        pw = PrintWriter(BufferedWriter(fil))
        pw!!.println("@256")
        pw!!.println("D=A")
        pw!!.println("@SP")
        pw!!.println("M=D")
    }

    /**
     * 算術コードを書き込む
     */
    fun writeArithmetic(command: String) {
        when (command) {
            "add" -> {
                decrementPointer()

                // 一つ目のスタック取得
                pw!!.println("@SP")
                pw!!.println("A=M")
                pw!!.println("D=M")
                pw!!.println("// 一つ目")


                decrementPointer()

                // 二つ目のスタック取得
                pw!!.println("@SP")
                pw!!.println("A=M")
                pw!!.println("M=M+D")
                pw!!.println("// 計算")

                incrementPointer()
            }
            "sub" -> {
                // 一つ目のスタック取得
                pw!!.println("@SP")
                pw!!.println("D=M")

                decrementPointer()

                // 二つ目のスタック取得
                pw!!.println("@SP")
                // 計算
                pw!!.println("M=M+D")
            }
            "neg" -> {

            }
            "eq" -> {

            }
            "gt" -> {

            }
            "lt" -> {

            }
            "and" -> {

            }
            "or" -> {

            }
            "not" -> {

            }

        }

    }

    /**
     * pushまたはpopコマンドを書き込む
     */
    fun writePushPop(command: Parser.COMMAND_TYPE, segment: String, index: Int) {
        if (command == Parser.COMMAND_TYPE.C_PUSH) {
            if (segment == "constant") {
                pw!!.println("// push start " + segment + "," + index.toString())

                pw!!.println("@" + index.toString())
                pw!!.println("D=A")
                pw!!.println("@SP")
                pw!!.println("A=M")
                pw!!.println("M=D")

                pw!!.println("// push end " + segment + "," + index.toString())

                incrementPointer()

                pw!!.println("// increment end")
                return
            }
            throw Exception()
        }
    }

    /**
     * スタックを一つ動かす
     */
    private fun incrementPointer() {
        pw!!.println("// increment start")
        pw!!.println("@SP")
        pw!!.println("M=M+1")
        pw!!.println("// increment end")
    }

    /**
     * スタックを一つ戻す
     */
    private fun decrementPointer() {
        pw!!.println("//  decrement start")
        pw!!.println("@SP")
        pw!!.println("M=M-1")
        pw!!.println("//  decrement end")
    }

    fun close() {
        pw!!.println("(END)")
        pw!!.println("@END")
        pw!!.println("0;JMP")
        pw!!.close()
    }
}