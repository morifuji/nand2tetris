import java.io.BufferedWriter
import java.io.FileWriter
import java.io.PrintWriter


fun main(args: Array<String>) {
    val parser = Parser("/Users/civiluo-1/nand2tetris/projects/07/StackArithmetic/StackTest/StackTest.vm")

    val codeWriter = CodeWriter("/Users/civiluo-1/nand2tetris/projects/07/StackArithmetic/StackTest/StackTest.asm")

    while (parser.hasMoreCommands()) {
        parser.advance()


        if (parser.commandType() === Parser.COMMAND_TYPE.C_PUSH) {
            codeWriter.writePushPop(Parser.COMMAND_TYPE.C_PUSH, parser.arg1(), Integer.parseInt(parser.arg2()))
        } else if (parser.commandType() === Parser.COMMAND_TYPE.C_ARITHMETIC) {
            codeWriter.writeArithmetic(parser.arg1())
        } else {
            throw Exception()
        }
    }
    codeWriter.close()
}
//
//val parser = Parser("/Users/civiluo-1/nand2tetris/projects/06/rect/Rect.asm")
//
//// 出力
//val fil = FileWriter("/Users/civiluo-1/nand2tetris/projects/06/rect/Rect.hack")
//
//// シンボルテーブル
//val symbolTable = SymbolTable()
//
//// 1. 変数保存
//while (parser.hasMoreCommands()) {
//    parser.advance()
//    if (parser.commandType() == Parser.COMMAND_TYPE.L_COMMAND) {
//        // 次の行のbinaryIndexを保存
//        symbolTable.addEntry(parser.symbol(), parser.binaryIndex + 1)
//    } else {
//        parser.binaryIndex++
//    }
//}
//
//parser.reset()
//
//// 2. 変換出力
//val pw = PrintWriter(BufferedWriter(fil))
//
//while (parser.hasMoreCommands()) {
//    parser.advance()
//    if (parser.commandType() === Parser.COMMAND_TYPE.NONE) {
//        continue
//    }
//
//    if (parser.commandType() === Parser.COMMAND_TYPE.L_COMMAND) {
//        continue
//    }
//
//    if (parser.commandType() === Parser.COMMAND_TYPE.A_COMMAND) {
//
//        val symbol = parser.symbol()
//
//        // シンボルに文字が含まれる場合
//        val address = if (symbol.contains(Regex("""\D+"""))) {
//
//            // シンボルテーブルに含まれていない場合
//            if (!symbolTable.contains(symbol)) {
//                // 新たに追加
//                symbolTable.addEntry(symbol)
//            }
//
//            // シンボルテーブルからアドレスを解決
//            symbolTable.getAddress(symbol)
//        } else {
//            // シンボルが数値の場合
//            Integer.parseInt(symbol)
//        }
//
//        pw.println("%016d".format(Integer.toBinaryString(address).toLong()))
//        continue
//    }
//
//    // C命令
//    val binaryString = "111" + Code.comp(parser.comp()) + Code.dest(parser.dest()) + Code.jump(parser.jump())
//    pw.println(binaryString)
//}
//
//pw.close()