import java.io.File
import java.io.InputStream


class Parser(filePath: String) {

    enum class COMMAND_TYPE {
        C_ARITHMETIC,
        C_PUSH,
        C_POP,
        NONE,
    }

    private var inputLines = mutableListOf<String>()

    private var commandIndex: Int = -1

    init {
        val inputStream: InputStream = File(filePath).inputStream()
        val lineList = mutableListOf<String>()
        inputStream.bufferedReader().useLines { lines -> lines.forEach { lineList.add(it) } }
        lineList.forEach {
            // コメントは除去
            val formattedLine = it.split("//")[0].trim()

            if (formattedLine.isEmpty()) {
                return@forEach
            }
            this.inputLines.add(formattedLine)
        }
    }


    // 次のコマンドを読み込む
    fun advance() {
        this.commandIndex++
    }

    fun commandType(): Parser.COMMAND_TYPE {
        val command = this.inputLines[commandIndex]

        if (listOf("add", "sub", "neg", "eq", "gt", "lt", "and", "or", "not").contains(command)) {
            return COMMAND_TYPE.C_ARITHMETIC
        }

        if (command.startsWith("push")) {
            return COMMAND_TYPE.C_PUSH
        }

        if (command.startsWith("pop")) {
            return COMMAND_TYPE.C_POP
        }

        return COMMAND_TYPE.NONE
    }

    fun arg1(): String {
        val command = this.inputLines[commandIndex]

        if (commandType() === COMMAND_TYPE.C_ARITHMETIC) {
            return command.split(" ")[0]
        }

        return command.split(" ")[1]
    }

    fun arg2(): String {
        val command = this.inputLines[commandIndex]

        return command.split(" ")[2]
    }

    fun hasMoreCommands():Boolean {
        return this.inputLines.size > this.commandIndex + 1
    }
}