こんにちは。

つづけて２章いきましょう。

[https://www.oreilly.co.jp/books/9784873117126/:embed:cite]

## 2章 ブール算術

#### オーバーフロー

nビット同士の加算によって、繰り上がりビットが発生すること。発生した繰り上がりビットをオーバーフロービットとも呼ぶ

#### 符号付２進数

0~7を表す２進数は、3ビットで事足りる `111=7`

これにマイナスを考慮する場合は、先頭に1ビットを付け加えて、そのビットが0=>正。1=>負とする

例えば、

- `0111=7`
- `0000=0`
- `1111=-1`
- `1001=-7`

#### 正負変換

全反転させて-1

`0111`=>`1000`=>`1001`

#### 半加算器

ふたつのビットの和を求める

#### 全加算器

みっつのビットの和を求める

#### 加算器

２つのnビットの和を求める

#### ALU

基本的な算術ができる汎用的な演算器。コンピュータによって実装が異なる。

掛け算・割り算などはここで実装しないが、OS側で実装する。

OSとALUがそれぞれどこの算術までカバーするかがキモ



#### 出力は複数指定できる

これは知らなかった。こちらの記事で気づきました。ありがとうございます :bow:

> http://blog.tojiru.net/article/426464326.html



### 実装

ALUくそ難しかった。

zrとngが厄介。

#### zr

計算結果が0の場合に1を返す。

`out=0000000000000000`の場合は、

1. `00000000`と`00000000`に分け(`out1`, `out2`)
2. 前半後半でそれぞれorで`全て0`か`1が一つでも存在しているか`チェック(`temp1-zr`, `temp2-zr`)
3. 前半後半それぞれの結果をor(`notzr`)
4. `notzr=1`なら0ではない、Notで反転(`zr`)

```vhdl
	Or8Way(in=out1, out=temp1-zr);
    Or8Way(in=out2, out=temp2-zr);
    Or(a=temp1-zr, b=temp2-zr, out=notzr);
    Not(in=notzr, out=zr);
```



#### ng

符号付２進数で紹介したが、先頭のビットが1なら負の数。よって、合計値を計算したさいの、`out[15]=1`なら負の数すなはち`ng=1`

```vhdl
Mux16(a=temp-result, b=temp-noresult, sel=no, out=out, out[0..7]=out1, out[8..15]=out2, out[15]=ng);
```



#### 僕の回答

```vhdl
// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/02/ALU.hdl

/**
 * The ALU (Arithmetic Logic Unit).
 * Computes one of the following functions:
 * x+y, x-y, y-x, 0, 1, -1, x, y, -x, -y, !x, !y,
 * x+1, y+1, x-1, y-1, x&y, x|y on two 16-bit inputs, 
 * according to 6 input bits denoted zx,nx,zy,ny,f,no.
 * In addition, the ALU computes two 1-bit outputs:
 * if the ALU output == 0, zr is set to 1; otherwise zr is set to 0;
 * if the ALU output < 0, ng is set to 1; otherwise ng is set to 0.
 */

// Implementation: the ALU logic manipulates the x and y inputs
// and operates on the resulting values, as follows:
// if (zx == 1) set x = 0        // 16-bit constant
// if (nx == 1) set x = !x       // bitwise not
// if (zy == 1) set y = 0        // 16-bit constant
// if (ny == 1) set y = !y       // bitwise not
// if (f == 1)  set out = x + y  // integer 2's complement addition
// if (f == 0)  set out = x & y  // bitwise and
// if (no == 1) set out = !out   // bitwise not
// if (out == 0) set zr = 1
// if (out < 0) set ng = 1

CHIP ALU {
    IN  
        x[16], y[16],  // 16-bit inputs        
        zx, // zero the x input?
        nx, // negate the x input?
        zy, // zero the y input?
        ny, // negate the y input?
        f,  // compute out = x + y (if 1) or x & y (if 0)
        no; // negate the out output?

    OUT 
        out[16], // 16-bit output
        zr, // 1 if (out == 0), 0 otherwise
        ng; // 1 if (out < 0),  0 otherwise

    PARTS:
    Mux16(a=x, sel=zx, out=tempx0);
    Mux16(a=y, sel=zy, out=tempy0);
 
    Not16(in=tempx0, out=notx);
    Not16(in=tempy0, out=noty);
  
    Mux16(a=tempx0, b=notx, sel=nx, out=tempx1);
    Mux16(a=tempy0, b=noty, sel=ny, out=tempy1);

    Add16(a=tempx1, b=tempy1, out=add16);
    And16(a=tempx1, b=tempy1, out=and16);

    Mux16(a=and16, b=add16, sel=f, out=temp-result);

    Not16(in=temp-result, out=temp-noresult);

    Mux16(a=temp-result, b=temp-noresult, sel=no, out=out, out[0..7]=out1, out[8..15]=out2, out[15]=ng);

    Or8Way(in=out1, out=temp1-zr);
    Or8Way(in=out2, out=temp2-zr);
    Or(a=temp1-zr, b=temp2-zr, out=notzr);
    Not(in=notzr, out=zr);
}
```

