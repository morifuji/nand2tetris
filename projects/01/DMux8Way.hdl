// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/DMux8Way.hdl

/**
 * 8-way demultiplexor:
 * {a, b, c, d, e, f, g, h} = {in, 0, 0, 0, 0, 0, 0, 0} if sel == 000
 *                            {0, in, 0, 0, 0, 0, 0, 0} if sel == 001
 *                            etc.
 *                            {0, 0, 0, 0, 0, 0, 0, in} if sel == 111
 */

CHIP DMux8Way {
    IN in, sel[3];
    OUT a, b, c, d, e, f, g, h;

    PARTS:
    Not(in=sel[0], out=notsel-0);
    Not(in=sel[1], out=notsel-1);
    Not(in=sel[2], out=notsel-2);

    // 000
    And(a=notsel-0, b=notsel-1, out=temp-a-1);
    And(a=notsel-2, b=temp-a-1, out=temp-a-2);
    And(a=temp-a-2, b=in, out=a);

    // 001
    And(a=sel[0], b=notsel-1, out=temp-b-1);
    And(a=notsel-2, b=temp-b-1, out=temp-b-2);
    And(a=temp-b-2, b=in, out=b);

    // 010
    And(a=notsel-0, b=sel[1], out=temp-c-1);
    And(a=notsel-2, b=temp-c-1, out=temp-c-2);
    And(a=temp-c-2, b=in, out=c);

    // 011
    And(a=sel[0], b=sel[1], out=temp-d-1);
    And(a=notsel-2, b=temp-d-1, out=temp-d-2);
    And(a=temp-d-2, b=in, out=d);

    // 100
    And(a=notsel-0, b=notsel-1, out=temp-e-1);
    And(a=sel[2], b=temp-e-1, out=temp-e-2);
    And(a=temp-e-2, b=in, out=e);

    // 101
    And(a=sel[0], b=notsel-1, out=temp-f-1);
    And(a=sel[2], b=temp-f-1, out=temp-f-2);
    And(a=temp-f-2, b=in, out=f);

    // 110
    And(a=notsel-0, b=sel[1], out=temp-g-1);
    And(a=sel[2], b=temp-g-1, out=temp-g-2);
    And(a=temp-g-2, b=in, out=g);

    // 111
    And(a=sel[0], b=sel[1], out=temp-h-1);
    And(a=sel[2], b=temp-h-1, out=temp-h-2);
    And(a=temp-h-2, b=in, out=h);
}