// 初期化
// 最大サイズの計算
@8192
D=A
@SCREEN
D=D+A
@MAXADDRESS //最大スクリーンアドレス
M=D

// 以下、無限ループ
(KEY)

@SCREEN
D=A
@address // スクリーンアドレス初期化
M=D

@KBD //キーボード取得
D=M

// 振り分け
@WHITE
D;JEQ
@BLACK
0;JMP

(WHITE)
@color
M=0
@LOOP
0;JMP

(BLACK)
@color
M=-1
@LOOP
0;JMP

// スクリーン書き換え
(LOOP)
@color
D=M

@address
A=M // 値のアドレスに移動
M=D //アドレスの値をcolorに

D=A+1 //アドレスに1加えたところに移動
@address // 必要！！！
M=D // 次に移動するために新たなアドレスを値として保存

@MAXADDRESS
D=M-D //Dが0かどうか

@LOOP
D;JNE

@KEY
0;JMP
(END)





// @8192
// D=A
// @count
// M=D

// @color
// M=0

// (LOOP)

// @index
// M=0

// // 振り分け
// @KBD
// D=M
// @WHITE
// D;JEQ


// (BLACK)
// @color
// M=1

// @INNER
// 0;JMP


// (WHITE)
// @color
// M=1
// // M=0


// // 描画ループ
// (INNER)

// // @color
// // D=M
// // @SCREEN
// // M=D

// // アドレス計算
// // @SCREEN
// // D=A
// // @index
// // D=M+D
// // @address
// // A=D

// // @color
// // D=M
// // @address
// // M=D

// // しるし
// 1;JEQ

// // // 描写
// // @color
// // D=M
// // @address
// // M=D

// // 加算
// @index
// M=M+1
// D=M

// // ifチェック
// @count
// D=M
// @index
// D=D-M
// @INNER
// D;JNE

// // 最初から
// @LOOP
// 0;JMP


// //         @8192   // (512 * 32) / 16
// //         D=A
// //         @count
// //         M=D     // count = 8192 (# of bytes)
// // (LOOP)
// //         @index
// //         M=0     // index = 0
// // (INNER)
// //         @KBD
// //         D=M
// //         @WHITE
// //         D;JEQ   // goto WHITE if KBD value is 0
// // (BLACK)
// //         @index
// //         D=M
// //         @SCREEN
// //         A=A+D   // Calculate byte address
// //         M=-1    // Fill with black
// //         @END
// //         0;JMP   // goto END
// // (WHITE)
// //         @index
// //         D=M
// //         @SCREEN
// //         A=A+D   // Calculate byte address
// //         M=0     // Fill with white
// // (END)   
// //         @index
// //         MD=M+1  // Increment index by 1
// //         @count
// //         D=D-M
// //         @LOOP
// //         D;JEQ   // goto LOOP if count - index == 0
// //         @INNER
// //         0;JMP   // goto INNER